/**
 * Created by Vladimir on 24.08.2015.
 */
package events {
import observer.IEventObserver;

public interface IWGtzEvents {
    function Add(event_id:String, observer:IEventObserver):void;
    function Remove(event_id:String, observer:IEventObserver):void;
    function Dispatch(event_id:String, event_data:Object):void;
}
}
