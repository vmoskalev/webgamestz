/**
 * Created by Vladimir on 24.08.2015.
 */
package events {
import flash.utils.Dictionary;

public class WGtzEventsOld {
    private static var _handlers:Dictionary = new Dictionary();
    private static var NOTNULL:String = "anydata";

    private static function getHandlersByEventID(id:String):Dictionary
    {
        return _handlers[id] ||= new Dictionary();
    }

    public static function Add(event_id:String, callback:Function)
    {
        getHandlersByEventID(event_id)[callback] = NOTNULL;
    }

    public static function Remove(event_id:String, callback:Function)
    {
        getHandlersByEventID(event_id)[callback] = null;
    }

    public static function Dispatch(event_id:String, event_data:Object):void {
        var handlers:Dictionary = getHandlersByEventID(event_id);
        for (var key:Function in handlers)
            if (handlers[key] == NOTNULL) key(event_data);
    }
}
}
