/**
 * Created by Vladimir on 24.08.2015.
 */
package events {
import flash.utils.Dictionary;

import observer.IEventObserver;

public class WGtzEvents implements IWGtzEvents{
    private var _handlers:Dictionary = new Dictionary();
    private var NOTNULL:String = "anydata";

    private static var _inst:IWGtzEvents;

    public static function get instance():IWGtzEvents
    {
        return _inst ||= new WGtzEvents(new Locker()) as IWGtzEvents;
    }

    public function WGtzEvents(locker:Locker) {
        if (!locker) throw new Error("IT IS SINGLETON!")
    }

    private function getHandlersByEventID(id:String):Dictionary
    {
        return _handlers[id] ||= new Dictionary(true);
    }

    public function Add(event_id:String, observer:IEventObserver):void
    {
        getHandlersByEventID(event_id)[observer] = NOTNULL;
    }

    public function Remove(event_id:String, observer:IEventObserver):void
    {
        getHandlersByEventID(event_id)[observer] = null;
    }

    public function Dispatch(event_id:String, event_data:Object):void {
        var handlers:Dictionary = getHandlersByEventID(event_id);
        for (var observer:IEventObserver in handlers)
            if (handlers[observer] == NOTNULL) observer.call(event_data);
    }
}
}

class Locker {
    public function Locker() {
    }
}