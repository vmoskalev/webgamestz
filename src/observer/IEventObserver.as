/**
 * Created by Vladimir on 24.08.2015.
 */
package observer {
public interface IEventObserver {
    function call(obj:Object);
}
}
