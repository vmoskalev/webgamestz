/**
 * Created by Vladimir on 24.08.2015.
 */
package person {
import flash.display.MovieClip;
import flash.display.Sprite;
import flash.events.Event;
import flash.geom.Point;

public class WGtzPersonPresentation extends Sprite implements IWGtzPersonPresentation{
    private var _drawPerson:Function;
    private var _targetX:Number;
    private var _targetY:Number;

    public function WGtzPersonPresentation() {
        super();

        graphics.clear();
        graphics.beginFill(0xff0000);
        graphics.drawCircle(10,10,20);
        graphics.endFill();

        addEventListener(Event.ENTER_FRAME, enterFrameHandler);
    }

    public function doMove(x:Number, y:Number):void {
        _targetX = x;
        _targetY = y;
    }

    private function enterFrameHandler(event:Event):void {
        var dx:Number = _targetX - x;
        var dy:Number = _targetY - y;
        var dist = Math.sqrt(dx*dx + dy*dy);
        dx /= dist;
        dy /= dist;
        x += dx;
        y += dy;
    }
}
}
