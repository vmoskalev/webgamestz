/**
 * Created by Vladimir on 24.08.2015.
 */
package person {
import observer.IEventObserver;

public interface IWGtzPerson extends IEventObserver {
    function get presentation():IWGtzPersonPresentation;
}
}
