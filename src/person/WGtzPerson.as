/**
 * Created by Vladimir on 24.08.2015.
 */
package person {
import events.WGtzEvents;

import flash.display.MovieClip;

public class WGtzPerson implements IWGtzPerson {
    private var _presentation:IWGtzPersonPresentation;

    public function get presentation():IWGtzPersonPresentation {
        return _presentation;
    }

    public function set presentation(value:IWGtzPersonPresentation):void {
        _presentation = value;
    }

    private var _serverX:int;
    private var _serverY:int;

    public function WGtzPerson(presentation:IWGtzPersonPresentation) {
        this.presentation = presentation;

        WGtzEvents.instance.Add("move", this);
    }

    public function call(obj:Object) {
        _serverX = obj.x;
        _serverY = obj.y;
        presentation.doMove(_serverX, _serverY);
    }
}
}
