package {

import events.WGtzEvents;

import flash.display.Sprite;
import flash.events.TimerEvent;
import flash.utils.Timer;

import person.WGtzPerson;

import person.WGtzPersonPresentation;

public class Main extends Sprite {
    var person:WGtzPerson;

    public function Main() {
        var presentation:WGtzPersonPresentation = new WGtzPersonPresentation();
        person = new WGtzPerson(presentation);
        addChild(presentation);
        var timer:Timer = new Timer(100);
        timer.addEventListener(TimerEvent.TIMER, timer_timerHandler);
        timer.start();
    }

    private function timer_timerHandler(event:TimerEvent):void {
        WGtzEvents.instance.Dispatch("move", {x:mouseX, y:mouseY});
    }
}
}
